# -*- coding: utf-8 -*-
import scrapy
from scrapy.shell import inspect_response
from scrapy.utils.response import open_in_browser

from fb_scrapy import items


class MbasicSpider(scrapy.Spider):
    name = 'mbasic'
    allowed_domains = ['mbasic.facebook.com']
    parsed_pages_count = 0

    def start_requests(self):
        return [scrapy.Request('http://mbasic.facebook.com/', callback=self.log_in)]

    def log_in(self, response):
        # login/pass come as spider args
        self.logger.info(f'{self.login}, {self.password}')
        return scrapy.FormRequest.from_response(
            response,
            formdata={'email': self.login, 'pass': self.password},
            callback=self.after_login
        )

    def after_login(self, response):
        if self.authentication_failed(response):
            self.logger.error("Login failed")
            return

        self.logger.info('Successfully logged in')
        # search keyword comes as a spider arg
        return scrapy.Request(f'https://mbasic.facebook.com/search/top/?q={self.keyword}')

    def parse(self, response):
        for item_div in response.xpath('.//table[@class="l bw"]'):
            name = item_div.css('div.cd::text').get()
            image = item_div.xpath('.//img/@src').get()
            yield items.FbItem(name=name, image_urls=[image])

        self.parsed_pages_count += 1

        next_page = response.xpath('//div[@id="see_more_pager"]/a/@href').get()
        self.logger.debug(next_page)
        page_limit = self.settings.getint('PAGE_LIMIT', 3)
        if next_page is None or self.parsed_pages_count >= page_limit:
            return

        self.logger.info(f'Fetching next page {self.parsed_pages_count+1}')
        yield scrapy.Request(next_page)

    def authentication_failed(self, response):
        err_text = response.xpath('string(//*[@id="login_error"])').get()
        if err_text:
            self.logger.error(err_text)
        return err_text != ''


