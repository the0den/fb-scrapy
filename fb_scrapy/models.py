from mongoengine import Document
from mongoengine.fields import StringField, BinaryField

class FbItem(Document):
    name = StringField()
    image_data = BinaryField()
