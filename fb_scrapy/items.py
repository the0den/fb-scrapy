# -*- coding: utf-8 -*-

import scrapy


class FbItem(scrapy.Item):
    name = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()
    image_data = scrapy.Field()
