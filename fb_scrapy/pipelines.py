# -*- coding: utf-8 -*-
import base64
import os
from pathlib import Path

from scrapy.exceptions import DropItem
from fb_scrapy.models import FbItem
from mongoengine import connect, disconnect

class ImageCachePipeline(object):
    def process_item(self, item, spider):
        if len(item['images']) < 1:
            raise DropItem(f'No images in {item}')

        for image in item['images']:
            abs_path = os.path.join(spider.settings.get('IMAGES_STORE'), image['path'])
            item['image_data'] = base64.b64encode(Path(abs_path).read_bytes())

        return item

class MongoDBPipeline(object):
    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI', 'mongodb://localhost'),
            mongo_db=crawler.settings.get('MONGO_DB', 'fb-scrapy')
        )

    def open_spider(self, spider):
        connect(self.mongo_db, host=self.mongo_uri)

    def close_spider(self, spider):
        disconnect()

    def process_item(self, item, spider):
        model = FbItem(name=item['name'], image_data=item['image_data'])
        model.save()

        spider.logger.info(item)
        return item
