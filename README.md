# Task

Write a scraping application using the `scrapy` library.

- It should get login, password and search keyword as parameters.
- Using the credentials provided, the app logs in through the mbasic.facebook.com.
- It searches for a provided keyword and scrapes titles and photos (base64) of the result elements.
- The scraped data should be persisted using MongoDB.

# Solution

Project requires pyenv and poetry to be installed.

```shell
# install the python requirements
poetry install

# lanch mongodb in a docker container
./run-mongo.sh

# start crawling
poetry shell
scrapy crawl mbasic -a login={fb_login} -a password={fb_password} -a keyword={search_keyword}

# to check scraped items
docker exec -it scrapy-mongo mongo

use fb-scrapy
db.fb_item.count()
db.fb_item.find()
```